package example.scala.demos

object DemoList {
  def main(args: Array[String]) {
    // Creates 5 elements using the given function.
    val list1 = List.range(10, 20)
    val list2 = List.range(25, 30)
    val number = 50

    println("List 1 \n" + list1)
    println("List 2 \n" + list2)
    println("Number \n" + number)
    println()

    val result0 = "Hola mundo " + list1 ++ list2
    println("Operation + \n" + result0)
    val result1 = list2 ++ list1
    println("Operation ++ \n" + result1)
    val result2 = list2 :: list1
    println("Operation :: with list \n" + result2)
    val result3 = number :: list1
    println("Operation :: with number \n" + result3)
    val result4 = list2 ::: list1
    println("Operation ::: \n" + result4)

    println("Operation apply with 5 \n" + list1.apply(5))

    println("Operation contains with 15 \n" + list1.contains(15))
    println("Operation contains with 25 \n" + list1.contains(25))

    val array = new Array[Int](5);
    list1.copyToArray(array, 1, 4)
    println("Operation copy to array \n" + array.mkString(","))
    val result5 = list1 ++ List.range(15, 25)
    println("Operation distinct \n" + result5.distinct)

    println("Operation drop 7 \n" + list1.drop(7))
    println("Operation take 3 \n" + list1.take(3))
    println("Operation drop right 7 \n" + list1.dropRight(7))
    println("Operation take right 3 \n" + list1.takeRight(3))
    println("Operation drop while p < 15 \n" + list1.dropWhile(p => p < 15))
    println("Operation take while p < 15 \n" + list1.takeWhile(p => p < 15))

    println("Operation startsWith with Seq 11-14, 1 \n" + list1.startsWith(Seq.range(11, 15), 1))
    println("Operation startsWith with Seq 11-14, 5 \n" + list1.startsWith(Seq.range(10, 15), 5))
    println("Operation endsWith with Seq 17-19 \n" + list1.endsWith(Seq.range(17, 20)))
    println("Operation endsWith with Seq 12-16 \n" + list1.endsWith(Seq.range(12, 17)))

    println("Operation equals Seq 25-29 \n" + list2.equals(List.range(25, 30)))
    println("Operation equals Seq 24-28 \n" + list2.equals(List.range(24, 29)))

    println("Operation exists with p + 5 = 20 \n" + list1.exists(p => p + 5 == 20))
    println("Operation exists with p + 5 = 50 \n" + list1.exists(p => p + 5 == 50))

    println("Operation filter with p%2 = 0 \n" + list1.filter(p => (p % 2) == 0))

    println("Operation forAll p > 9 \n" + list1.forall(p => p > 9))
    println("Operation forAll p > 12 \n" + list1.forall(p => p > 12))

    println("Operation forEach p * 3")
    list1.foreach(p => print(p * 3 + " - "))
    println()

    println("Operation head for list1 \n" + list1.head)
    println("Operation head for list2 \n" + list2.head)

    println("Operation last for list1 \n" + list1.last)
    println("Operation last for list2 \n" + list2.last)

    println("Operation length for list1 \n" + list1.length)
    println("Operation length for list2 \n" + list2.length)

    println("Operation min for list1 \n" + list1.min)
    println("Operation min for list2 \n" + list2.min)

    println("Operation max for list1 \n" + list1.max)
    println("Operation max for list2 \n" + list2.max)

    println("Operation sum for list1 \n" + list1.sum)
    println("Operation sum for list2 \n" + list2.sum)

    println("Operation init for list1 \n" + list1.init)
    println("Operation init for list2 \n" + list2.init)

    println("Operation reversed for list1 \n" + list1.reverse)
    println("Operation reversed for list2 \n" + list2.reverse)

    println("Operation indexOf 10, fom 0 \n" + list1.indexOf(10))
    println("Operation indexOf 10, fom 5 \n" + list1.indexOf(10, 5))
    println("Operation lastIndexOf 18, fom 0 \n" + list1.lastIndexOf(18))
    println("Operation lastIndexOf 16, fom 5 \n" + list1.lastIndexOf(16, 5))

    println("Operation intersect for Seq 13-17 \n" + list1.intersect(Seq.range(13, 18)))
    println("Operation intersect for Seq 18-28 \n" + list1.intersect(Seq.range(18, 29)))

    val sorted = list1.sortWith(_ % 2 < _ % 2)
    println("Operation sorted i1 % 2 > i2 % 2 \n" + sorted)

    val doubles = list1.map(p => (p * 2).toString)
    println("Operation map p*2 to String \n" + doubles)

    val map = list1.map(p => {
      val string = if ((p % 2) == 0) "pair" else "uneven"
      (p, string)
    }).toMap
    println(map)
  }
}