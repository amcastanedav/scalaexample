package example.scala.demos

/**
  * Created by acastanedav on 16/01/17.
  */
object DemoMap {
  def main(args: Array[String]): Unit = {
    val map1 = Map("Amazonas" -> "Leticia", "Antioquia" -> "Medellín", "Arauca" -> "Arauca")
    val map2 = Map("Caldas" -> "Manizales", "Casanare" -> "Yopal", "Huila" -> "Neiva")

    println("Map1 \n" + map1)
    println("Map2 \n" + map2)

    var result = map1 ++ map2
    println("Operation ++ \n" + result)
    result = map1.++(map2)
    println("Operation .++ \n" + result)

    println("Keys \n" + result.keys)
    println("Values \n" + result.values)

    println("Contains Caldas \n" + result.contains("Caldas"))
    println("Contains Cundinamarca \n" + result.contains("Cundinamarca"))

    println("Count Key start with 'A' \n" + result.count(p => p._1.charAt(0) == 'A'))
    println("Count Value start with 'M' \n" + result.count(p => p._2.charAt(0) == 'M'))

    println("Exist Key equals Value \n" + result.exists(p => p._1.equals(p._2)))
    println("Exist Key not equals Value \n" + result.exists(p => !p._1.equals(p._2)))
    println("Exist first Key character equals third Value character \n" + result.exists(p => p._1.charAt(0) == p._2.charAt(2)))

    println("Filter Key equals Value \n" + result.filter(p => p._1.equals(p._2)))
    println("Filter Key not equals Value \n" + result.filter(p => !p._1.equals(p._2)))
    println("Filter Key start with 'A' \n" + result.filterKeys(p => p.charAt(0) == 'A'))

    println("Find Key start with 'A' \n" + result.find(p => p._1.charAt(0) == 'A'))
    println("Find Value start with 'A' \n" + result.find(p => p._2.charAt(0) == 'A'))
    println("Find Key start with 'Z' \n" + result.find(p => p._1.charAt(0) == 'K'))

    result.foreach(f => println(f._2 + " is the capital of " + f._1))

    println("Operation last \n" + result.last)
    println("Operation min \n" + result.min)
    println("Operation max \n" + result.max)
  }
}
