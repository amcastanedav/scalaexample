package example.scala.demos

/**
  * Created by acastanedav on 16/01/17.
  */
object DemoSet {
  def main(args: Array[String]): Unit = {
    val set1 = List.range(15, 25).toSet
    val set2 = List.range(20, 30).toSet

    println("Set1 \n" + set1)
    println("Set2 \n" + set2)

    val result1: Set[Int] = set1 ++ set2
    println("Operation ++ \n" + result1)
    val result2: Set[Int] = set1.++(set2)
    println("Operation .++ \n" + result2)

    println("Min in set1 \n" + set1.min)
    println("Min in set2 \n" + set2.min)
    println("Max in set1 \n" + set1.max)
    println("Max in set2 \n" + set2.max)

    println("Operation .& \n" + set1.&(set2))
    println("Operation intersect \n" + set1.intersect(set2))

    println("Operation +25 list1 \n" + set1.+(25))
    println("Operation +24 list1 \n" + set1.+(24))
    println("Operation -25 list1 \n" + set1.-(25))
    println("Operation -24 list1 \n" + set1.-(24))

    println("Operation .&~ \n" + set1.&~(set2))
    println("Operation diff \n" + set1.diff(set2))

    println("Operation find (p + 5) == 27 \n" + set1.find(p => (p + 5) == 27))
    println("Operation find (p + 5) == 30 \n" + set1.find(p => (p + 5) == 30))

    println("Operation product set1 \n" + set1.product)
    println("Operation product set2 \n" + set2.product)

    println("Operation split set1 \n" + set1.splitAt(3))
    println("Operation split set2 \n" + set2.splitAt(3))

    val setTest = Set(24, 20, 18)
    println("Operation subset for (24,20,18) set1 \n" + setTest.subsetOf(set1))
    println("Operation subset for (24,20,18) set2 \n" + setTest.subsetOf(set2))
  }
}
