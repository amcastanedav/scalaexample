package example.scala.demos

/**
  * Created by acastanedav on 17/01/17.
  */
object DemoOption {
  def main(args: Array[String]): Unit = {
    val map = Map(1 -> "one", 2 -> "two", 3 -> "three")
    val option1 = map.get(1)
    val option4 = map.get(4)

    println("Map \n" + map)
    println("Option for 1 \n" + option1)
    println("Option for 4 \n" + option4)

    println("Product Arity 1 \n" + option1.productArity)
    println("Product Arity 4 \n" + option1.productArity)

    println("Exist option1 charAt 0 'o' \n" + option1.exists(p => p.charAt(0) == 'o'))
    println("Exist option1 charAt 0 'a' \n" + option1.exists(p => p.charAt(0) == 'a'))
    println("Exist option2 charAt 0 'a' \n" + option4.exists(p => p.charAt(0) == 'a'))

    println("Filter option1 charAt 0 'o' \n" + option1.filter(p => p.charAt(0) == 'o'))
    println("Filter option1 charAt 0 'a' \n" + option1.filter(p => p.charAt(0) == 'a'))
    println("Filter option2 charAt 0 'a' \n" + option4.filter(p => p.charAt(0) == 'a'))

    println("Filter not option1 charAt 0 'o' \n" + option1.filterNot(p => p.charAt(0) == 'o'))
    println("Filter not option1 charAt 0 'a' \n" + option1.filterNot(p => p.charAt(0) == 'a'))
    println("Filter not option2 charAt 0 'a' \n" + option4.filterNot(p => p.charAt(0) == 'a'))

    println("Get or else option1 \n" + option1.getOrElse("empty"))
    println("Get or else option4 \n" + option4.getOrElse("empty"))

    println("Else option1 map.get(5) \n" + option1.orElse(map.get(5)))
    println("Else option4 map.get(2) \n" + option4.orElse(map.get(2)))

    println("IsDefined option1 \n" + option1.isDefined)
    println("IsDefined option4 \n" + option4.isDefined)

    option1.foreach(f => println("Value option1 " + f))
    option4.foreach(f => println("Value option4 " + f))
  }
}
