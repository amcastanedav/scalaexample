package example.scala.demos

/**
  * Created by acastanedav on 17/01/17.
  */
object DemoIterator {
  def main(args: Array[String]) {
    val ita = Iterator(20, 40, 2, 50, 69, 90)
    val itb = Iterator(15, 40, 7, 48, 66, 95)

    val it = ita.++(itb)
    while (it.hasNext){
      println("Print with while " + it.next())
    }

    val list = List(20,40,2,50,69, 90)

    println("Maximum valued element " + list.iterator.max)
    println("Minimum valued element " + list.iterator.min)
    println("Size " + list.iterator.size)
    println("Length " + list.iterator.length)
  }
}
