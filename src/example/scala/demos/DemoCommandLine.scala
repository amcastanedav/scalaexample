package example.scala.demos

/**
  * Created by acastanedav on 18/01/17.
  */
object DemoCommandLine {
  def main(args: Array[String]) {
    print("Please enter your input : " )
    val line = io.StdIn.readLine()

    println("Thanks, you just typed: " + line)
  }
}
