package example.scala.demos

/**
  * Created by acastanedav on 17/01/17.
  */
object DemoTuple {
  def main(args: Array[String]) {
    val t1 = (4, 3, 2, 1)

    t1.productIterator.foreach { i => println("Value = " + i) }

    val t2 = new Tuple2("Scala", "hello")

    println("Swapped Tuple: " + t2.swap )
  }
}
