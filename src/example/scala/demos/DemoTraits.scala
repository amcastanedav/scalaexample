package example.scala.demos

/**
  * Created by acastanedav on 17/01/17.
  */
trait years {
  var humanYears : Int
  def actualYears(): Int
}

class Cat(hy:Int) extends years {
  override var humanYears: Int = hy
  override def actualYears(): Int = {
    if (humanYears <= 0)
      return 0
    if (humanYears == 1)
      return 15
    if (humanYears == 2)
      return 24
    (humanYears - 2) * 4 + 24
  }
}

class Dog(hy:Int) extends years {
  override var humanYears : Int = hy
  override def actualYears(): Int = {
    if (humanYears <= 0)
      return 0
    if (humanYears == 1)
      return 15
    if (humanYears == 2)
      return 24
    if (humanYears >= 3 && humanYears <= 5)
      return (humanYears - 2) * 4 + 24
    if (humanYears == 6)
      return 45
    (humanYears - 6) * 5 + 45
  }
}

class Developer(hy:Int) extends years {
  override var humanYears : Int = hy
  override def actualYears(): Int = {
    humanYears / 4
  }
}

object DemoTraits {
  def main(args: Array[String]): Unit = {
    val cat = new Cat(7)
    val dog = new Dog(9)
    val developer = new Developer(7)

    println("A cat with " + cat.humanYears + " human years has " + cat.actualYears() + " cat years")
    println("A dog with " + dog.humanYears + " human years has " + dog.actualYears() + " dog years")
    println("A developer with " + developer.humanYears + " human years has " + developer.actualYears() + " developer years")
  }
}
