package example.scala.demos

/**
  * Created by acastanedav on 18/01/17.
  */
object DemoRegularExpressions {
  def main(args: Array[String]) {
    val pattern = """(\w+)@([\w\.]+)""".r
    val mail1 = "acastanedav@hola.com"
    val mail2 = "acastanedav@ hola.com"
    val mail3 = "acastanedav @hola.com"

    println(mail1 + " is a email? " + (pattern unapplySeq mail1).isDefined)
    println(mail2 + " is a email? " + (pattern unapplySeq mail2).isDefined)
    println(mail3 + " is a email? " + (pattern unapplySeq mail3).isDefined)
  }
}
