package example.scala.demos

/**
  * Created by acastanedav on 18/01/17.
  */
case class Quadrant(xi: Int, yi: Int, name:String) {
  val x: Int = xi
  val y: Int = yi
  val n:String = name
}

object DemoMatch {
  def main(args: Array[String]) {
    val facebook = new Quadrant(1, 1, "facebook")
    val twitter = new Quadrant(-1, 1, "twitter")
    val linkedin = new Quadrant(-1, -1, "linkedin")

    for (social <- List(facebook, twitter, linkedin)) {
      social match {
        case Quadrant(0, 0, name) => println(name + " is in the origin")
        case Quadrant(1, 1, name) => println(name + " is a Leader")
        case Quadrant(-1, 1, name) => println(name + " is a Challenger")
        case Quadrant(-1, -1, name) => println(name + " is a Niche Player")
        case Quadrant(1, -1, name) => println(name + " is a Visionary")
        case Quadrant(xi, yi, name) => println("Hi " + name + " is in " + xi + ", " + yi)
      }
    }
  }
}