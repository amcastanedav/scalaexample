package example.scala.numbers

import scala.Array._

/**
  * Created by acastanedav on 12/01/17.
  */
class Basic {

  private val horizontal = "-"
  private val vertical = "|"
  private val empty = " "

  def blank(size: Int): Array[Array[String]] = {
    fill(1, size + 2)(empty)
  }

  def line(size: Int): Array[Array[String]] = {
    val line = fill(1, size + 2)(horizontal)
    line(0)(0) = empty
    line(0)(size + 1) = empty
    line
  }

  def blankMatrix(size: Int): Array[Array[String]] = {
    fill(size, size + 2)(empty)
  }

  def right(size: Int): Array[Array[String]] = {
    val right = blankMatrix(size)
    for (i <- 0 until size) {
      right(i)(size + 1) = vertical
    }
    right
  }

  def left(size: Int): Array[Array[String]] = {
    val left = blankMatrix(size)
    for (i <- 0 until size) {
      left(i)(0) = vertical
    }
    left
  }

  def both(size: Int): Array[Array[String]] = {
    val both = blankMatrix(size)
    for (i <- 0 until size) {
      both(i)(0) = vertical
      both(i)(size + 1) = vertical
    }
    both
  }
}
