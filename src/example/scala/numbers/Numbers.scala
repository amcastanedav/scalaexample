package example.scala.numbers

/**
  * Created by acastanedav on 12/01/17.
  */
class Numbers {

  val basic = new Basic()

  def one(size:Int): Array[Array[String]] = {
    val lineUp = basic.blank(size)
    val bodyUp = basic.right(size)
    val lineMiddle = basic.blank(size)
    val bodyDown = basic.right(size)
    val lineDown = basic.blank(size)
    lineUp ++ bodyUp ++lineMiddle ++ bodyDown ++ lineDown
  }

  def two(size:Int): Array[Array[String]] = {
    val lineUp = basic.line(size)
    val bodyUp = basic.right(size)
    val lineMiddle = basic.line(size)
    val bodyDown = basic.left(size)
    val lineDown = basic.line(size)
    lineUp ++ bodyUp ++lineMiddle ++ bodyDown ++ lineDown
  }

  def three(size:Int): Array[Array[String]] = {
    val lineUp = basic.line(size)
    val bodyUp = basic.right(size)
    val lineMiddle = basic.line(size)
    val bodyDown = basic.right(size)
    val lineDown = basic.line(size)
    lineUp ++ bodyUp ++lineMiddle ++ bodyDown ++ lineDown
  }

  def four(size:Int): Array[Array[String]] = {
    val lineUp = basic.blank(size)
    val bodyUp = basic.both(size)
    val lineMiddle = basic.line(size)
    val bodyDown = basic.right(size)
    val lineDown = basic.blank(size)
    lineUp ++ bodyUp ++lineMiddle ++ bodyDown ++ lineDown
  }

  def five(size:Int): Array[Array[String]] = {
    val lineUp = basic.line(size)
    val bodyUp = basic.left(size)
    val lineMiddle = basic.line(size)
    val bodyDown = basic.right(size)
    val lineDown = basic.line(size)
    lineUp ++ bodyUp ++lineMiddle ++ bodyDown ++ lineDown
  }

  def six(size:Int): Array[Array[String]] = {
    val lineUp = basic.line(size)
    val bodyUp = basic.left(size)
    val lineMiddle = basic.line(size)
    val bodyDown = basic.both(size)
    val lineDown = basic.line(size)
    lineUp ++ bodyUp ++lineMiddle ++ bodyDown ++ lineDown
  }

  def seven(size:Int): Array[Array[String]] = {
    val lineUp = basic.line(size)
    val bodyUp = basic.right(size)
    val lineMiddle = basic.blank(size)
    val bodyDown = basic.right(size)
    val lineDown = basic.blank(size)
    lineUp ++ bodyUp ++lineMiddle ++ bodyDown ++ lineDown
  }

  def eight(size:Int): Array[Array[String]] = {
    val lineUp = basic.line(size)
    val bodyUp = basic.both(size)
    val lineMiddle = basic.line(size)
    val bodyDown = basic.both(size)
    val lineDown = basic.line(size)
    lineUp ++ bodyUp ++lineMiddle ++ bodyDown ++ lineDown
  }

  def nine(size:Int): Array[Array[String]] = {
    val lineUp = basic.line(size)
    val bodyUp = basic.both(size)
    val lineMiddle = basic.line(size)
    val bodyDown = basic.right(size)
    val lineDown = basic.line(size)
    lineUp ++ bodyUp ++lineMiddle ++ bodyDown ++ lineDown
  }

  def zero(size:Int): Array[Array[String]] = {
    val lineUp = basic.line(size)
    val bodyUp = basic.both(size)
    val lineMiddle = basic.blank(size)
    val bodyDown = basic.both(size)
    val lineDown = basic.line(size)
    lineUp ++ bodyUp ++lineMiddle ++ bodyDown ++ lineDown
  }
}
