package example.scala.numbers

/**
  * Created by acastanedav on 11/01/17.
  */
object Demo {
  def main(args: Array[String]) {
    val numbers = new Numbers()
    val size = 3

    println("\n\nOne")
    printNumber(numbers.one(size))
    println("\n\nTwo")
    printNumber(numbers.two(size))
    println("\n\nThree")
    printNumber(numbers.three(size))
    println("\n\nFour")
    printNumber(numbers.four(size))
    println("\n\nFive")
    printNumber(numbers.five(size))
    println("\n\nSix")
    printNumber(numbers.six(size))
    println("\n\nSeven")
    printNumber(numbers.seven(size))
    println("\n\nEight")
    printNumber(numbers.eight(size))
    println("\n\nNine")
    printNumber(numbers.nine(size))
    println("\n\nZero")
    printNumber(numbers.zero(size))
  }

  def printNumber(toPrint: Array[Array[String]]): Unit = {
    for (line <- toPrint){
      for (c <- line) {
        print(c)
      }
      println()
    }
  }
}