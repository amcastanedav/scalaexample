# Scala Examples

This repository contains different examples of scala functions:
- Lists
- Maps
- Options
- Sets
- Iterators
- Tuples
- Traits
- Match

## Context

A brief explanation of the exercises can be found in [Tutorials Point](https://www.tutorialspoint.com/scala/scala_pattern_matching.htm)

## Use the Demos

You have to compile the project to use the different demos

## Out

Each demo has a different console output
