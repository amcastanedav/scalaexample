package example.scala.demos

import org.scalatest._

/**
  * Created by acastanedav on 18/01/17.
  */
class DemoTraitsTest extends FlatSpec {

  "A cat with 0 years" should "have 0 actual years" in {
    val cat = new Cat(0)
    assertResult(0)(cat.actualYears())
  }

  "A cat with 1 year" should "have 15 actual years" in {
    val cat = new Cat(1)
    assertResult(15)(cat.actualYears())
  }

  "A cat with 2 years" should "have 24 actual years" in {
    val cat = new Cat(2)
    assertResult(24)(cat.actualYears())
  }

  "A cat with 3 years" should "have 28 actual years" in {
    val cat = new Cat(3)
    assertResult(28)(cat.actualYears())
  }

  "A cat with 10 years" should "have 56 actual years" in {
    val cat = new Cat(10)
    assertResult(56)(cat.actualYears())
  }

  "A cat with 15 years" should "have 76 actual years" in {
    val cat = new Cat(15)
    assertResult(76)(cat.actualYears())
  }

  "A dog with 0 years" should "have 0 actual years" in {
    val dog = new Dog(0)
    assertResult(0)(dog.actualYears())
  }

  "A dog with 1 year" should "have 15 actual years" in {
    val dog = new Dog(1)
    assertResult(15)(dog.actualYears())
  }

  "A dog with 2 years" should "have 24 actual years" in {
    val dog = new Dog(2)
    assertResult(24)(dog.actualYears())
  }

  "A dog with 3 years" should "have 28 actual years" in {
    val dog = new Dog(3)
    assertResult(28)(dog.actualYears())
  }

  "A dog with 4 years" should "have 32 actual years" in {
    val dog = new Dog(4)
    assertResult(32)(dog.actualYears())
  }

  "A dog with 5 years" should "have 35 actual years" in {
    val dog = new Dog(5)
    assertResult(36)(dog.actualYears())
  }

  "A dog with 6 years" should "have 45 actual years" in {
    val dog = new Dog(6)
    assertResult(45)(dog.actualYears())
  }

  "A dog with 7 years" should "have 50 actual years" in {
    val dog = new Dog(7)
    assertResult(50)(dog.actualYears())
  }

  "A dog with 8 years" should "have 55 actual years" in {
    val dog = new Dog(8)
    assertResult(55)(dog.actualYears())
  }

  "A dog with 9 years" should "have 61 actual years" in {
    val dog = new Dog(9)
    assertResult(60)(dog.actualYears())
  }

  "A dog with 10 years" should "have 66 actual years" in {
    val dog = new Dog(10)
    assertResult(65)(dog.actualYears())
  }

  "A developer with 0 years" should "have 0 actual years" in {
    val developer = new Developer(0)
    assertResult(0)(developer.actualYears())
  }

  "A developer with 2 years" should "have 0 actual years" in {
    val developer = new Developer(2)
    assertResult(0)(developer.actualYears())
  }

  "A developer with 4 years" should "have 1 actual years" in {
    val developer = new Developer(4)
    assertResult(1)(developer.actualYears())
  }

  "A developer with 20 years" should "have 5 actual years" in {
    val developer = new Developer(20)
    assertResult(5)(developer.actualYears())
  }
}
