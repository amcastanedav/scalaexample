package example.scala.numbers

import org.scalatest._

/**
  * Created by acastanedav on 18/01/17.
  */
class BasicTest extends FlatSpec {

  "A blank" should "return array with spaces" in {
    val r = scala.util.Random
    val basic = new Basic()
    val size = r.nextInt(50)
    val blank = basic.blank(size)
    assert(blank.isInstanceOf[Array[Array[String]]])
    assertResult(1)(blank.length)
    assertResult(size + 2)(blank(0).length)
    for (part <- blank(0))
      assertResult(" ")(part)
  }

  "A line" should "return array with lines" in {
    val r = scala.util.Random
    val basic = new Basic()
    val size = r.nextInt(50)
    val line = basic.line(size)
    assert(line.isInstanceOf[Array[Array[String]]])
    assertResult(1)(line.length)
    assertResult(size + 2)(line(0).length)
    assertResult(" ")(line(0)(0))
    assertResult(" ")(line(0)(size + 1))
    for (i <- 1 to size) {
      assertResult("-")(line(0)(i))
    }
  }

  "A right" should "return a matrix with pipes in the right" in {
    val r = scala.util.Random
    val basic = new Basic()
    val size = r.nextInt(50)
    val right = basic.right(size)
    assert(right.isInstanceOf[Array[Array[String]]])
    assertResult(size)(right.length)
    assertResult(size + 2)(right(0).length)
    for (line <- right) {
      for (i <- 0 to size)
        assertResult(" ")(line(i))
    }
    for (i <- 1 until size)
      assertResult("|")(right(i)(size + 1))
  }

  "A left" should "return a matrix with pipes in the left" in {
    val r = scala.util.Random
    val basic = new Basic()
    val size = r.nextInt(50)
    val left = basic.left(size)
    assert(left.isInstanceOf[Array[Array[String]]])
    assertResult(size)(left.length)
    assertResult(size + 2)(left(0).length)
    for (line <- left) {
      for (i <- 1 to size + 1)
        assertResult(" ")(line(i))
    }
    for (i <- 1 until size)
      assertResult("|")(left(i)(0))
  }

  "A both" should "return a matrix with pipes in the right and in the left" in {
    val r = scala.util.Random
    val basic = new Basic()
    val size = r.nextInt(50)
    val both = basic.both(size)
    assert(both.isInstanceOf[Array[Array[String]]])
    assertResult(size)(both.length)
    assertResult(size + 2)(both(0).length)
    for (line <- both) {
      for (i <- 1 to size)
        assertResult(" ")(line(i))
    }
    for (i <- 1 until size) {
      assertResult("|")(both(i)(0))
      assertResult("|")(both(i)(size + 1))
    }
  }

}
